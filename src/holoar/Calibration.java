/*
 * Copyright (C) 2017 Mher Tolpin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package holoar;




import java.util.List;
import java.util.ArrayList;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.core.Size;
import static org.opencv.highgui.Highgui.imwrite;
import org.opencv.imgproc.Imgproc;


/**
 *
 * @author Mher Tolpin
 */
public class Calibration {

    /** 
     * Camera calibration function 
     * @param frame - the frame to calibrate 
     * @return the camera calibration data 
     */
    public CalibrateData calibrate(Mat frame)
    {
        Mat gray = new Mat();                                       // the Mat of gray image 
        Mat cameraMatrix = new Mat();                               // Camera Matrix , to store calibrated matrix
        Mat distCoeffs = new Mat();                                 // Matrix to store calibrated matrix
        MatOfPoint3f points;
        
        // convertion frame to gray scale
        Imgproc.cvtColor(frame, gray, Imgproc.COLOR_RGB2GRAY);      
        
        Size patternSize = new Size(7,6);                           // the chessboard size
        MatOfPoint2f actual_corners = new MatOfPoint2f();           // the chessboard corners
        List<Mat> corners= new ArrayList<Mat>();                    // corners list
        List<Mat> object_points= new ArrayList<Mat>();              // objp list 
        List<Mat> rvecs= new ArrayList<Mat>();                      // calibration data
        List<Mat> tvecs= new ArrayList<Mat>();                      // calibration data 
        
        // trying to find chess board on image 
        boolean found_chess = Calib3d.findChessboardCorners(gray, patternSize, actual_corners,  Calib3d.CALIB_CB_ADAPTIVE_THRESH + Calib3d.CALIB_CB_NORMALIZE_IMAGE);
        
        if(found_chess)
        // if chess board found 
        {
            // adding the found corners to 'corners' list 
            corners.add(actual_corners);
            
            // creating the objp array 
            Mat a = new MatOfPoint3f();
                for(int y=0; y<6; ++y) 
                {
                    for(int x=0; x<7; ++x)
                    {
                        points = new MatOfPoint3f(new Point3(x, y, 0));
                        a.push_back(points);
                    }
                }
                
            object_points.add(a);
            
            // calibrating the cam
            Calib3d.calibrateCamera(object_points, corners, gray.size(), cameraMatrix, distCoeffs, rvecs, tvecs);
            
            // storing the data 
            CalibrateData data = new CalibrateData(cameraMatrix, distCoeffs, rvecs, tvecs);
            
            // returning data if found 
            return data;
         }
        
        // returing 'null' if not calibrated 
        return null;
    }
    
    
}
