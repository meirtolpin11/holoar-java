/*
 * Copyright (C) 2017 Mher Tolpin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package holoar;

import java.awt.EventQueue;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.opencv.core.Core;
import org.opencv.core.Mat;

/**
 * The Main Class of the project, Extends JFrame and Creates a cam window 
 * @author Mher Tolpin
 */
public class HoloAR extends JFrame{

    private JPanel contentPane;

    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
    /**
     * @param args - NO ARGUMENTS 
     */
    
    static Webcam cam;                                  // static variable of video cam 
    static CalibrateData data = null;                   // storing the camera calibration data 
    static Mat2Image mat2Img = new Mat2Image();         // Mat to Image convertor 
    static Calibration cal ;                            // Calibration class
    static CameraPose pose;                             // Camera proccessing class
    static Mat prevFrame;                               // storing the previous frame 
    static boolean found;                               // true is chessboard found, else false 
    int counter = 0;
   
    public static void main(String[] args) {
        // TODO starting the camera, calibration, and proccessing every frame 
        cam = new Webcam();                             // the camera class
        cam.start();                                    // starting camera thread
        
        Mat frame = new Mat();                          
        
        while(data == null)
        // Until not calibrated
        {
            do
            {
                // until frame is empty
              frame = cam.get_current_Mat();  
            }while(frame.empty());
            
            // Calibrating the camera 
            cal = new Calibration();                     
            data = cal.calibrate(frame);
            
            // initialaizing the previous frame 
            prevFrame = frame;
        }
        
        // Creating proccessing class
        pose = new CameraPose(data.cameraMatrix, data.distCoeffs);
        
        // printing that the camera was callibrated, the data stored in 'data' 
        System.out.println("Calibrated");
 
        // Creating a Window 
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    HoloAR frame = new HoloAR();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    /** 
    * Create the frame.
    */
    public HoloAR() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 650, 490);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
  
        new MyThread().start();
    }
 
    
    @Override
    public void paint(Graphics g){
        g = contentPane.getGraphics();
        
        // getting the frame
        Mat frame = cam.get_current_Mat();
        
        // if the frame is not empty
        if( frame != null)
        {
                // processing image
                frame = pose.processImgCube(frame, data.cameraMatrix, data.distCoeffs);   

                
                if(!found)
                {
                   counter++;
                   System.out.println("no chess");
                }
                else
                {
                    System.out.println("Chess");
                    counter = 0;
                }
                prevFrame = frame;
        }
        // showing the image 
        g.drawImage(mat2Img.getImage(prevFrame), 0, 0, this); 
        
        if(counter>=20)
        {
            System.out.println("calibrate");
            do
            {
                //data = cal.calibrate(frame);
            }while(data==null);
            counter = 0;
        }
    }
 
    class MyThread extends Thread{
        @Override
        public void run() {
            for (;;){
                repaint();
                try { Thread.sleep(30);
                } catch (InterruptedException e) {    }
            }  
        } 
    }
    
    
}
