/*
 * Copyright (C) 2017 Mher Tolpin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package holoar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * CameraPose class, here we process the image
 * @author Mher Tolpin
 */
public class CameraPose {
    static Mat mtx, dist, frame;
    static Double prevSlope=null;
    static Point prevStart= null, prevCoordinates= null;
    static Mat prevFrame;
    static MatOfPoint2f PrevActualCorners;
    static MatOfPoint2f Previmgpts;
    int moveX = 2;
    int counter = 0;
    
    public CameraPose(Mat mtx,Mat dist)
    {
        // storing the input data 
        this.mtx = mtx;
        this.dist = dist;
    }
    
    /** 
     * Mathematical functions class, here is the logic of moving object left or right 
     * I calculate the slope of every vector, choosing the lowest slope, calculating
     * the new coordinates, and moving the object 
     * @param center - center coordinates
     * @param pointR - Red point coordinates
     * @param pointG - Green point coordinates 
     * @param pointB - Blue point coordinates 
     * @param corners - the list of corners the update 
     * @param imgpts  - the list of image points to update 
     */
    public void doM(Point center, Point pointR, Point pointG, Point pointB, Point[] corners, Point[] imgpts)
    {
        // vars 
        Point point = null;
        double slopeB, slopeG, slope;
        double centerX, centerY, distance, changeX, changeY;
        
        // backing up the X,Y coordinates of the center 
        centerX = center.x;
        centerY = center.y;
        
        // calculating the slopes
        slopeB = (pointB.y - center.y)/(pointB.x - center.x);
        slopeG = (pointG.y - center.y)/(pointG.x - center.x);
        
        // choosing the lowest slope and getting slope sign
        slope = Math.min(Math.abs(slopeB), Math.abs(slopeG));
        if (slope != slopeB && slope != slopeG)
        {
            slope = - slope;
        }
        
        // finding the point for function 
        if (slope == slopeB)
            point = pointB;
        else
            point = pointG;
        
        // updating start point 
        if (this.prevStart == null)
               this.prevStart = center.clone();
        
        // checking if moving right or left 
        if (slope > 0)
            moveX = Math.abs(moveX);
        else
            moveX = -Math.abs(moveX);
        
        // calculating the distance between the centers 
        distance = Math.sqrt(Math.pow((prevStart.x-center.x),2) + Math.pow((prevStart.y-center.y),2));
        
        // if not so far, setting to the previous coordinates 
        if (distance < 50 && this.prevCoordinates!= null)
        {
            center.x = prevCoordinates.x;
            center.y = prevCoordinates.y;
        }
        else
        {
            prevStart = center.clone();
        }
        
        // updating X,Y of the center (moving left or right) 
        center.y = functionY(slope, point, center.x + moveX);
        center.x += moveX;
        
        // updating the previous coordinates 
        prevCoordinates = center.clone();
        
        // calculation the diffrence 
        changeY = center.y - centerY;
        changeX = center.x - centerX;
        
        // updating other points
        for(int i = 1; i<corners.length; i++)
        {
            point = corners[i];
            point.x += changeX;
            point.y += changeY;
        }
        
        for(int i = 1; i<imgpts.length; i++)
        {
            point = imgpts[i];
            point.x += changeX;
            point.y += changeY;
        }
        
        moveX = Math.abs(moveX);
    }
    
    /** 
     * new point coordinates calculating 
     * @param slope - the function slope 
     * @param point - the function point
     * @param newX  - new X coordinate 
     * @return - the new Y coordinate 
     */
    public double functionY(double slope, Point point, double newX)
    {
        double y = slope*(newX - point.x) + point.y;
        return y;
    }
    
    /** 
     * Processing image and 
     * @param frame - the frame to process 
     * @param mtx  - the camera calibration data 
     * @param dist  - the camera calibration data
     * @return  - the processed Image 
     */
    public Mat processImg(Mat frame, Mat mtx, Mat dist)
    {
        // vars 
        MatOfPoint3f points;
        Mat tvec=new Mat();
        Mat rvec = new Mat();
        MatOfDouble dist1 = new MatOfDouble(dist);
        Mat gray = new Mat();
        MatOfPoint2f actual_corners = new MatOfPoint2f();
        MatOfPoint2f imgpts = new MatOfPoint2f();
        
        // size of the chessboard pattern 
        Size patternSize = new Size(7,6);
        
        // converting frame to gray scale 
        Imgproc.cvtColor(frame, gray, Imgproc.COLOR_BGR2GRAY);
        
        // creating objp points 
        MatOfPoint3f obj = new MatOfPoint3f();
                for(int y=0; y<6; ++y) 
                {
                    for(int x=0; x<7; ++x)
                    {
                        points = new MatOfPoint3f(new Point3(x, y, 0));
                        obj.push_back(points);
                    }
                }
                
        // creating imgp points 
        Point3 p1 = new Point3(3,0,0);
        Point3 p2 = new Point3(0,3,0);
        Point3 p3 = new Point3(0,0,-3);
        
        // contains the imgp list 
        Point3[] test = {p3,p2,p1};
        
        MatOfPoint3f objectPoints= new MatOfPoint3f();
        MatOfPoint3f axis = new MatOfPoint3f();
        
        // axis of the vectors 
        axis.fromArray(test);
        
        // trying to find chessboard 
        boolean found_chess = Calib3d.findChessboardCorners(gray, patternSize, actual_corners,  Calib3d.CALIB_CB_ADAPTIVE_THRESH + Calib3d.CALIB_CB_NORMALIZE_IMAGE);
        
        if(found_chess)
        // if chessboard was found 
        {
            HoloAR.found = true;
            
            // calibrating
            Calib3d.solvePnP(obj, actual_corners, mtx, dist1, rvec, tvec);
            Calib3d.projectPoints(axis, rvec, tvec, mtx, dist1, imgpts);
            
            // drawing vectors 
            frame = drawVectors(frame, actual_corners, imgpts);
            
        }
        else
        {
            // not relevant 
            HoloAR.found = false;
        }
        return frame;
    }
    
    /** 
     * drawing vectors on the Image 
     * @param frame - the frame to draw 
     * @param corners - the vector corners 
     * @param imgpts - vector points 
     * @return - updated frame
     */
    public Mat drawVectors(Mat frame, MatOfPoint2f corners, MatOfPoint2f imgpts)
    {
        Point[] imgpts1 = imgpts.toArray();
        Point[] corners1 = corners.toArray();
        
        
        doM(corners1[0], imgpts1[0], imgpts1[1], imgpts1[2], corners1, imgpts1);
        
        Core.line(frame, corners1[0], imgpts1[0], new Scalar(255,0,0), 5);
        Core.line(frame, corners1[0], imgpts1[1], new Scalar(0,255,0), 5);
        Core.line(frame, corners1[0], imgpts1[2], new Scalar(0,0,255), 5);
        return frame;
    }
    
    /** 
     * Drawing a cube on the Image 
     * @param frame - the frame to draw 
     * @param corners - the vector corners 
     * @param imgpts - vector points 
     * @return - updated frame
     */
    public Mat drawCube(Mat frame, MatOfPoint2f corners, MatOfPoint2f imgpts)
    {
        // vars
        Point[] imgpts1 = imgpts.toArray();
        Point[] corners1 = corners.toArray();
        
        // calculating new points 
        doM(imgpts1[0], imgpts1[2],imgpts1[3],imgpts1[1], corners1, imgpts1);
        
        /* calculating point lists */
        List<MatOfPoint> posDown = new ArrayList<MatOfPoint>();
        MatOfPoint down = new MatOfPoint();
        down.fromArray(Arrays.copyOfRange(imgpts1, 0, 4));
        posDown.add(down);
        
        List<MatOfPoint> posUp = new ArrayList<MatOfPoint>();
        MatOfPoint up = new MatOfPoint();
        up.fromArray(Arrays.copyOfRange(imgpts1, 4, 8));
        posUp.add(up);
        
        /****************************/
        
        // drawing the up and down of the cube 
        //Core.polylines(frame, posDown, true, new Scalar(255,0,0), 3);
        Core.polylines(frame, posUp, true, new Scalar(0,255,0), 3);
        Core.fillPoly(frame, posDown, new Scalar(255,0,0));
        
        // drawing the connecting lines 
        Core.line(frame, imgpts1[0], imgpts1[4], new Scalar(0,0,255),3);
        Core.line(frame, imgpts1[1], imgpts1[5], new Scalar(0,0,255),3);
        Core.line(frame, imgpts1[2], imgpts1[6], new Scalar(0,0,255),3);
        Core.line(frame, imgpts1[3], imgpts1[7], new Scalar(0,0,255),3);
        
        return frame;
    }
    
    /** 
     * Processing Image and drawing a cube 
     * @param frame - the frame to process 
     * @param mtx  - the camera calibration data 
     * @param dist  - the camera calibration data
     * @return  - the processed Image 
     */
    public Mat processImgCube(Mat frame, Mat mtx, Mat dist)
    {
        // vars
        MatOfPoint3f points;
        Mat tvec=new Mat();
        Mat rvec = new Mat();
        MatOfDouble dist1 = new MatOfDouble(dist);
        Mat gray = new Mat();
        MatOfPoint2f actual_corners = new MatOfPoint2f();
        MatOfPoint2f imgpts = new MatOfPoint2f();
        
        // chessboard pattern size 
        Size patternSize = new Size(7,6);
        
        // converting to gray scale 
        Imgproc.cvtColor(frame, gray, Imgproc.COLOR_BGR2GRAY);
        
        /* creating obj and imgp points */
        MatOfPoint3f obj = new MatOfPoint3f();
                for(int y=0; y<6; ++y) 
                {
                    for(int x=0; x<7; ++x)
                    {
                        points = new MatOfPoint3f(new Point3(x, y, 0));
                        obj.push_back(points);
                    }
                }
                
        Point3 p1 = new Point3(0,0,0);
        Point3 p2 = new Point3(0,3,0);
        Point3 p3 = new Point3(3,3,0);
        Point3 p4 = new Point3(3,0,0);
        Point3 p5 = new Point3(0,0,-3);
        Point3 p6 = new Point3(0,3,-3);
        Point3 p7 = new Point3(3,3,-3);
        Point3 p8 = new Point3(3,0,-3);
       
        Point3[] test = {p1,p2,p3,p4,p5,p6,p7,p8};
       
        /**********************************/
        
        MatOfPoint3f objectPoints= new MatOfPoint3f();
        
        MatOfPoint3f axis = new MatOfPoint3f();
        axis.fromArray(test);
        
        // trying to find chessboard
        boolean found_chess = Calib3d.findChessboardCorners(gray, patternSize, actual_corners,  Calib3d.CALIB_CB_ADAPTIVE_THRESH + Calib3d.CALIB_CB_NORMALIZE_IMAGE);
        
        if(found_chess)
        // if found 
        {
            HoloAR.found = true;
            
            // calibrating
            Calib3d.solvePnP(obj, actual_corners, mtx, dist1, rvec, tvec);
            Calib3d.projectPoints(axis, rvec, tvec, mtx, dist1, imgpts);
            
            // drawing
            frame = drawCube(frame, actual_corners, imgpts);
            prevFrame = frame;
            Previmgpts = imgpts;
            PrevActualCorners = actual_corners;
        }
        else
        {
            if(prevFrame != null && counter < 10)
            {
                frame = drawCube(prevFrame, PrevActualCorners, Previmgpts);
                counter++;
            }
            else
            {
                prevFrame = null;
                counter = 0;
               
            }
            // not relevant , always true 
            HoloAR.found = false;
        }
        return frame;
    }
}

