/*
 * Copyright (C) 2017 Mher Tolpin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package holoar;

import java.awt.image.BufferedImage;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import static org.opencv.highgui.Highgui.imwrite;
import org.opencv.highgui.VideoCapture;

/**
 *  Camera Class, getting camera data 
 * @author Mher Tolpin
 */

public class Webcam {
    VideoCapture video;
    Mat current_frame;
    Mat2Image mat2Img = new Mat2Image();
    
    public Webcam()
    {
        current_frame = new Mat();              // creating the Mat
        video = new VideoCapture(0);            // starting capture 
        
    }
    
    /** 
     * starting the camera updater thread 
     */
    public void start()
    {
        final Webcam parent = this;
        Thread t = new Thread(){
            public void run()
            {
                // starting the updater 
                parent.update();
            }
            
        };
        t.start();
    }
    
    /** 
     * Getting new frames and updating the 'frame' var 
     */
    public void update()
    {
        while(true)
        {
            video.read(this.current_frame);
            
            //Core.flip(current_frame, current_frame, 1);
        }
    }
    
    // returns the current frame as BufferedImage 
    public BufferedImage get_current_frame()
    {
        return mat2Img.getImage(this.current_frame);
    }
    
    // return the current frame as Mat 
    public Mat get_current_Mat()
    {
        return this.current_frame;
    }
    
}
