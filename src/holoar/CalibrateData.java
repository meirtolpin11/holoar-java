/*
 * Copyright (C) 2017 Mher Tolpin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package holoar;

import java.util.List;
import org.opencv.core.Mat;

/**
 *
 * @author Mher Tolpin
 */
public class CalibrateData {
    List<Mat> rvecs;
    List<Mat> tvecs;
    Mat cameraMatrix;   // mtx
    Mat distCoeffs;     // dist 

    
    /** 
     * Data struct of calibration data 
    */
    public CalibrateData(Mat cameraMatrix, Mat distCoeffs, List<Mat> rvecs, List<Mat> tvecs)
    {
        this.cameraMatrix = cameraMatrix;
        this.distCoeffs = distCoeffs;
        this.rvecs = rvecs;
        this.tvecs = tvecs;
    }
}
